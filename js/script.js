"use strict";

console.log('Arr' + "-oject");
console.log(4 + "5");
console.log(4 + +"5");
// Постфиксная форма
/*
let incr = 10,
    decr = 10;
incr++;
decr--;
console.log(incr);
console.log(decr);
*/
// Префиксная форма
let incr = 10,
    decr = 10;
//++incr;
//--decr;
console.log(++incr);
console.log(--decr);
console.log(5%2);
console.log(2*4 === 8);

const   isChecked = true,
        isClose = false;
console.log(isChecked && isClose);
console.log(isChecked || !isClose);